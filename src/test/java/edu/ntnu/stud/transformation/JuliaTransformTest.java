package edu.ntnu.stud.transformation;

import edu.ntnu.stud.model.math.Complex;
import edu.ntnu.stud.model.math.Vector2D;
import edu.ntnu.stud.model.transformation.JuliaTransform;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

class JuliaTransformTest {
  @Test
  @DisplayName("Julia transform with positive sign")
  public void transformWithPositiveSign() {
    Complex complexPoint = new Complex(0.3,0.6);
    JuliaTransform julia = new JuliaTransform(complexPoint,1);
    Vector2D z = new Vector2D(0.4,0.2);
    Vector2D result = julia.transform(z);
    Assertions.assertEquals(0.506,result.getX0(),0.001);
    Assertions.assertEquals(-0.395,result.getX1(),0.001);
  }

  @Test
  @DisplayName("Julia transform with negative sign")
  public void transformWithNegativeSign() {
    Complex complexPoint = new Complex(0.3,0.6);
    JuliaTransform julia = new JuliaTransform(complexPoint,-1);
    Vector2D z = new Vector2D(0.4,0.2);
    Vector2D result = julia.transform(z);
    Assertions.assertEquals(-0.506,result.getX0(),0.001);
    Assertions.assertEquals(0.395,result.getX1(),0.001);
  }

  @Test
  @DisplayName("Should not create an object when sign is different than +-1")
  public void illegalJuliaTransformObject() {
    Complex complexPoint = new Complex(0.3,0.6);
    Assertions.assertThrows(IllegalArgumentException.class, () -> {
      JuliaTransform julia = new JuliaTransform(complexPoint,2);
    });
  }
}