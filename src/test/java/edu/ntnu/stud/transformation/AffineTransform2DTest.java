package edu.ntnu.stud.transformation;

import edu.ntnu.stud.model.math.Matrix2x2;
import edu.ntnu.stud.model.math.Vector2D;
import edu.ntnu.stud.model.transformation.AffineTransform2D;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

class AffineTransform2DTest {
  @Test
  public void transformPositive() {
    Matrix2x2 matrix2x2 = new Matrix2x2(1,2,3,4);
    Vector2D vector2D = new Vector2D(1,2);
    AffineTransform2D affineTransform2D = new AffineTransform2D(matrix2x2,vector2D);
    Vector2D result = affineTransform2D.transform(new Vector2D(3,4));
    Assertions.assertEquals(12,result.getX0());
    Assertions.assertEquals(27,result.getX1());
  }
}