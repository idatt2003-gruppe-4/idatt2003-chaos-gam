package edu.ntnu.stud.game;

import edu.ntnu.stud.model.game.ChaosCanvas;
import edu.ntnu.stud.model.math.Vector2D;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class ChaosCanvasTest {
  @Test
  @DisplayName("getCanvasArray returns the canvas correctly")
  void testGetCanvasArrayReturnsCorrectly() {
    try {
      ChaosCanvas chaosCanvas = new ChaosCanvas(101, 101, new Vector2D(0, 0),
          new Vector2D(100, 100));
      int[][] result = chaosCanvas.getCanvasArray();
      assertNotNull(result);
    } catch (Exception e) {
      fail("An exception was thrown with the message: " + e.getMessage());
    }
  }

  @Test
  @DisplayName("toString returns correct string")
  void testToStringReturnsCorrectString() {
    try {
      ChaosCanvas chaosCanvas = new ChaosCanvas(11, 11, new Vector2D(0, 0),
          new Vector2D(10, 10));
      chaosCanvas.putPixel(new Vector2D(0, 0));
      chaosCanvas.putPixel(new Vector2D(10, 10));
      String result = chaosCanvas.toString();
      assertNotNull(result);
      assertEquals("          X\n"
          + "           \n"
          + "           \n"
          + "           \n"
          + "           \n"
          + "           \n"
          + "           \n"
          + "           \n"
          + "           \n"
          + "           \n"
          + "X          \n", result);
    } catch (Exception e) {
      fail("An exception was thrown with the message: " + e.getMessage());
    }
  }


}
