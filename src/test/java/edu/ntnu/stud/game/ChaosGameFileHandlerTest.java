package edu.ntnu.stud.game;

import edu.ntnu.stud.model.game.ChaosGameDescription;
import edu.ntnu.stud.model.game.ChaosGameFileHandler;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.io.FileNotFoundException;

public class ChaosGameFileHandlerTest {
  @Test
  public void readFile() throws FileNotFoundException {
    ChaosGameDescription description = ChaosGameFileHandler.readFromFile("src/main/resources/Triangle.txt");
    Assertions.assertEquals(0,description.getMinCords().getX0());
    Assertions.assertEquals(0,description.getMinCords().getX1());
    Assertions.assertEquals(1,description.getMaxCords().getX0());
    Assertions.assertEquals(1,description.getMaxCords().getX1());
    Assertions.assertEquals(3,description.getTransforms().size());
  }

  @Test
  public void writeToFile() throws FileNotFoundException {
    ChaosGameDescription description = ChaosGameFileHandler.readFromFile("src/main/resources/Triangle.txt");
    String filepath = "src/main/resources";
    ChaosGameFileHandler.writeToFile(description,filepath);
  }

  @Test
  public void readFromWrittenFile() throws FileNotFoundException {
    ChaosGameDescription description = ChaosGameFileHandler.readFromFile("src/main/resources/Triangle.txt");
    Assertions.assertEquals(0,description.getMinCords().getX0());
    Assertions.assertEquals(0,description.getMinCords().getX1());
    Assertions.assertEquals(1,description.getMaxCords().getX0());
    Assertions.assertEquals(1,description.getMaxCords().getX1());
    Assertions.assertEquals(3,description.getTransforms().size());
  }

  @Test
  public void readFromFileJulia() throws FileNotFoundException {
    ChaosGameDescription description = ChaosGameFileHandler.readFromFile("src/main/resources/Julia.txt");
    Assertions.assertEquals(-1.6,description.getMinCords().getX0());
    Assertions.assertEquals(-1,description.getMinCords().getX1());
    Assertions.assertEquals(1.6,description.getMaxCords().getX0());
    Assertions.assertEquals(1,description.getMaxCords().getX1());
    Assertions.assertEquals(2,description.getTransforms().size());
  }
}
