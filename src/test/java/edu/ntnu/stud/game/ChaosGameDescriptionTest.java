package edu.ntnu.stud.game;

import edu.ntnu.stud.model.game.ChaosGameDescription;
import edu.ntnu.stud.model.math.Vector2D;
import edu.ntnu.stud.model.transformation.AffineTransform2D;
import edu.ntnu.stud.model.math.Matrix2x2;
import edu.ntnu.stud.model.transformation.Transform2D;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;

class ChaosGameDescriptionTest {

  @Test
  void getTransforms() {
    ArrayList<Transform2D> transforms = new ArrayList<>();
    transforms.add(new AffineTransform2D(new Matrix2x2(0.5, 0, 0, 0.5), new Vector2D(0, 0)));
    ChaosGameDescription description = new ChaosGameDescription(transforms, new Vector2D(0, 0), new Vector2D(1, 1));

    Assertions.assertEquals(transforms, description.getTransforms());
  }

  @Test
  void getMinCords() {
    Vector2D minCords = new Vector2D(0, 0);
    ChaosGameDescription description = new ChaosGameDescription(new ArrayList<>(), minCords, new Vector2D(1, 1));

    Assertions.assertEquals(minCords, description.getMinCords());
  }

  @Test
  void getMaxCords() {
    Vector2D maxCords = new Vector2D(1, 1);
    ChaosGameDescription description = new ChaosGameDescription(new ArrayList<>(), new Vector2D(0, 0), maxCords);

    Assertions.assertEquals(maxCords, description.getMaxCords());
  }

  @Test
  void testToString() {
    ArrayList<Transform2D> transforms = new ArrayList<>();
    transforms.add(new AffineTransform2D(new Matrix2x2(0.5, 0, 0, 0.5), new Vector2D(0, 0)));
    Vector2D minCords = new Vector2D(0, 0);
    Vector2D maxCords = new Vector2D(1, 1);
    ChaosGameDescription description = new ChaosGameDescription(transforms, minCords, maxCords);

    String expectedString = "Affine2D    # Type of transform\n" +
        minCords + "    # Lower left\n" +
        maxCords + "    # Upper right\n" +
        transforms.get(0) + "    # transform\n";

    Assertions.assertEquals(expectedString, description.toString());
  }

  @Test
  void testConstructorWithNullTransforms() {
    Exception exception = Assertions.assertThrows(IllegalArgumentException.class, () -> {
      new ChaosGameDescription(null, new Vector2D(0, 0), new Vector2D(1, 1));
    });

    String expectedMessage = "Transforms cannot be null";
    String actualMessage = exception.getMessage();

    Assertions.assertTrue(actualMessage.contains(expectedMessage));
  }

  @Test
  void testConstructorWithNullMinCords() {
    ArrayList<Transform2D> transforms = new ArrayList<>();
    transforms.add(new AffineTransform2D(new Matrix2x2(0.5, 0, 0, 0.5), new Vector2D(0, 0)));

    Exception exception = Assertions.assertThrows(IllegalArgumentException.class, () -> {
      new ChaosGameDescription(transforms, null, new Vector2D(1, 1));
    });

    String expectedMessage = "MinCords cannot be null";
    String actualMessage = exception.getMessage();

    Assertions.assertTrue(actualMessage.contains(expectedMessage));
  }

  @Test
  void testConstructorWithNullMaxCords() {
    ArrayList<Transform2D> transforms = new ArrayList<>();
    transforms.add(new AffineTransform2D(new Matrix2x2(0.5, 0, 0, 0.5), new Vector2D(0, 0)));

    Exception exception = Assertions.assertThrows(IllegalArgumentException.class, () -> {
      new ChaosGameDescription(transforms, new Vector2D(0, 0), null);
    });

    String expectedMessage = "MaxCords cannot be null";
    String actualMessage = exception.getMessage();

    Assertions.assertTrue(actualMessage.contains(expectedMessage));
  }
}