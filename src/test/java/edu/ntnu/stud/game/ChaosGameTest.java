package edu.ntnu.stud.game;

import edu.ntnu.stud.model.game.ChaosGame;
import edu.ntnu.stud.model.game.ChaosGameDescription;
import edu.ntnu.stud.model.math.Vector2D;
import edu.ntnu.stud.model.transformation.AffineTransform2D;
import edu.ntnu.stud.model.math.Matrix2x2;
import edu.ntnu.stud.model.transformation.Transform2D;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;

class ChaosGameTest {

  @Test
  void getCanvas() {

    Vector2D minCords = new Vector2D(0, 0);
    Vector2D maxCords = new Vector2D(1, 1);
    ArrayList<Transform2D> transforms = new ArrayList<>();
    transforms.add(new AffineTransform2D(new Matrix2x2(0.5, 0, 0, 0.5), new Vector2D(0, 0)));
    ChaosGameDescription description = new ChaosGameDescription(transforms, minCords, maxCords);

   ChaosGame chaosGame = new ChaosGame(description, 100, 100);
   Assertions.assertNotNull(chaosGame.getCanvas());
  }

  @Test
  void runSteps() {
    Vector2D minCords = new Vector2D(0, 0);
    Vector2D maxCords = new Vector2D(1, 1);
    ArrayList<Transform2D> transforms = new ArrayList<>();
    transforms.add(new AffineTransform2D(new Matrix2x2(0.5, 0, 0, 0.5), new Vector2D(0, 0)));
    ChaosGameDescription description = new ChaosGameDescription(transforms, minCords, maxCords);


    ChaosGame chaosGame = new ChaosGame(description, 100, 100);


    chaosGame.runSteps();


    Assertions.assertNotNull(chaosGame.getCanvas());
  }

  @Test
  void runStepsNegative() {
    Vector2D minCords = new Vector2D(0, 0);
    Vector2D maxCords = new Vector2D(1, 1);
    ArrayList<Transform2D> transforms = new ArrayList<>();
    transforms.add(new AffineTransform2D(new Matrix2x2(0.5, 0, 0, 0.5), new Vector2D(0, 0)));
    ChaosGameDescription description = new ChaosGameDescription(transforms, minCords, maxCords);

    ChaosGame chaosGame = new ChaosGame(description, 100, 100);

    // Call the runSteps method with a negative number of steps
    Exception exception = Assertions.assertThrows(IllegalArgumentException.class, () -> {
      chaosGame.updateSteps(-1000);
    });

    String expectedMessage = "Number of steps cannot be negative";
    String actualMessage = exception.getMessage();

    Assertions.assertTrue(actualMessage.contains(expectedMessage));
  }

  @Test
  void testConstructorWithNullDescription() {
    Exception exception = Assertions.assertThrows(IllegalArgumentException.class, () -> {
      new ChaosGame(null, 100, 100);
    });

    String expectedMessage = "Description cannot be null";
    String actualMessage = exception.getMessage();

    Assertions.assertTrue(actualMessage.contains(expectedMessage));
  }

  @Test
  void testConstructorWithZeroWidth() {
    Vector2D minCords = new Vector2D(0, 0);
    Vector2D maxCords = new Vector2D(1, 1);
    ArrayList<Transform2D> transforms = new ArrayList<>();
    transforms.add(new AffineTransform2D(new Matrix2x2(0.5, 0, 0, 0.5), new Vector2D(0, 0)));
    ChaosGameDescription description = new ChaosGameDescription(transforms, minCords, maxCords);

    Exception exception = Assertions.assertThrows(IllegalArgumentException.class, () -> {
      new ChaosGame(description, 0, 100);
    });

    String expectedMessage = "Width and height must be positive";
    String actualMessage = exception.getMessage();

    Assertions.assertTrue(actualMessage.contains(expectedMessage));
  }

  @Test
  void testConstructorWithNegativeHeight() {
    Vector2D minCords = new Vector2D(0, 0);
    Vector2D maxCords = new Vector2D(1, 1);
    ArrayList<Transform2D> transforms = new ArrayList<>();
    transforms.add(new AffineTransform2D(new Matrix2x2(0.5, 0, 0, 0.5), new Vector2D(0, 0)));
    ChaosGameDescription description = new ChaosGameDescription(transforms, minCords, maxCords);

    Exception exception = Assertions.assertThrows(IllegalArgumentException.class, () -> {
      new ChaosGame(description, 100, -100);
    });

    String expectedMessage = "Width and height must be positive";
    String actualMessage = exception.getMessage();

    Assertions.assertTrue(actualMessage.contains(expectedMessage));
  }
}