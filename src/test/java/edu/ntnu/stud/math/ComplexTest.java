package edu.ntnu.stud.math;

import edu.ntnu.stud.model.math.Complex;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ComplexTest {

    @Test
    void sqrt() {
        Complex complex = new Complex(1.0, 1.0);

        Complex result = complex.sqrt();

        assertEquals(1.09868, result.getX0(), 0.001);
        assertEquals(0.45509, result.getX1(), 0.001);
    }
}