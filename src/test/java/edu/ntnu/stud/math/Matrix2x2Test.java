package edu.ntnu.stud.math;

import edu.ntnu.stud.model.math.Matrix2x2;
import edu.ntnu.stud.model.math.Vector2D;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class Matrix2x2Test {

    @Test
    void multiply() {
        Matrix2x2 matrix = new Matrix2x2(1.0, 2.0, 3.0, 4.0);
        Vector2D vector = new Vector2D(5.0, 6.0);

        Vector2D result = matrix.multiply(vector);

        assertEquals(17.0, result.getX0(), 0.0001);
        assertEquals(39.0, result.getX1(), 0.0001);
    }
}