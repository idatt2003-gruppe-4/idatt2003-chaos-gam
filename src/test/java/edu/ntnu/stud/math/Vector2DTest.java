package edu.ntnu.stud.math;

import edu.ntnu.stud.model.math.Vector2D;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class Vector2DTest {

    @Test
    void getX0() {
        Vector2D vector = new Vector2D(3.0, 5.0);
        assertEquals(3.0, vector.getX0(), 0.0001);
    }

    @Test
    void getX1() {
        Vector2D vector = new Vector2D(3.0, 5.0);
        assertEquals(5.0, vector.getX1(), 0.0001);
    }

    @Test
    void add() {
        Vector2D vector1 = new Vector2D(3.0, 5.0);
        Vector2D vector2 = new Vector2D(1.0, 2.0);

        Vector2D result = vector1.add(vector2);

        assertEquals(4.0, result.getX0(), 0.0001);
        assertEquals(7.0, result.getX1(), 0.0001);
    }

    @Test
    void subtract() {
        Vector2D vector1 = new Vector2D(3.0, 5.0);
        Vector2D vector2 = new Vector2D(1.0, 2.0);

        Vector2D result = vector1.subtract(vector2);

        assertEquals(2.0, result.getX0(), 0.0001);
        assertEquals(3.0, result.getX1(), 0.0001);
    }
}