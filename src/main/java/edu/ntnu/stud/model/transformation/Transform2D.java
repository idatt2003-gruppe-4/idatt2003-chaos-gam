package edu.ntnu.stud.model.transformation;

import edu.ntnu.stud.model.math.Vector2D;

/**
 * Represents a 2D transformation, and implements the Transform2D
 * interface.
 * Contains a method for transforming a 2D vector.
 */
public interface Transform2D {
   Vector2D transform(Vector2D vector);
}
