package edu.ntnu.stud.model.transformation;

import edu.ntnu.stud.model.math.Matrix2x2;
import edu.ntnu.stud.model.math.Vector2D;

/**
 * Represents a 2D affine transformation ,
 * and implements the Transform2D interface.
 * Contains as well a 2x2 matrix and a 2D vector.
 */
public class AffineTransform2D implements Transform2D {
  private Matrix2x2 matrix;
  private Vector2D vector;

  /**
   * Constructor with the 2x2 matrix and the 2D vector.
   *
   * @param matrix the 2x2 matrix
   * @param vector the 2D vector
   */
  public AffineTransform2D(Matrix2x2 matrix, Vector2D vector) {
    this.matrix = matrix;
    this.vector = vector;
  }

  /**
   * Calculates x = matrix * point + vector
   *
   * @param point point used to multiply with the matrix
   * @return a new point calculated through the equation mentioned.
   */
  public Vector2D transform(Vector2D point) {
    Vector2D temp = matrix.multiply(point);
    return temp.add(vector);
  }

  /**
   * Returns a string representation of the AffineTransform2D.
   *
   * @return a string representation of the AffineTransform2D
   */
  @Override
  public String toString() {
    return matrix.toString() + ", " + vector.toString();
  }
}
