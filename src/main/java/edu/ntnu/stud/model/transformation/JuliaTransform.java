package edu.ntnu.stud.model.transformation;

import edu.ntnu.stud.model.math.Complex;
import edu.ntnu.stud.model.math.Vector2D;

/**
 * Represents a Julia transformation, and implements the
 * Transform2D interface.
 * Contains a complex number and a sign.
 */
public class JuliaTransform implements Transform2D {

  private Complex point;
  private int sign;

  /**
   * Constructor with the complex number and the sign.
   *
   * @param point the complex point of the transformation
   * @param sign the sign of the transformation, must be either
   *             -1 or 1
   * @throws IllegalArgumentException if the sign is not -1 or 1
   */
  public JuliaTransform(Complex point, int sign) {
    this.point = point;
    if (sign != -1 && sign != 1) {
      throw new IllegalArgumentException(
          "Sign has to be either +-1"
      );
    }
    this.sign = sign;
  }

  /**
   * Julia transformation of a given point
   *
   * @param vector the point to be transformed
   * @return the result of the transformation
   */
  public Vector2D transform(Vector2D vector) {
    vector = vector.subtract(point);
    Complex newComplexPoint = new Complex(vector.getX0(), vector.getX1());
    newComplexPoint = newComplexPoint.sqrt();

    return new Vector2D(sign * newComplexPoint.getX0(), sign * newComplexPoint.getX1());
  }

  /**
   * Returns a string representation of the JuliaTransform.
   *
   * @return a string representation of the JuliaTransform
   */
  @Override
  public String toString() {
    return point.toString();
  }
}
