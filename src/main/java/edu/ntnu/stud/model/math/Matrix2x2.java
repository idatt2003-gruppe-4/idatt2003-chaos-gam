package edu.ntnu.stud.model.math;

/**
 * This is the class Matrix2x2 which represents a
 * 2 by 2 matrix with methods for multiplying it
 * with a 2D vector.
 */
public class Matrix2x2 {
  private double a00;
  private double a01;
  private double a10;
  private double a11;

  /**
   * Constructor for the Matrix2x2 class.
   *
   * @param a00 the first element of the matrix
   * @param a01 the second element of the matrix
   * @param a10 the third element of the matrix
   * @param a11 the fourth element of the matrix
   */
  public Matrix2x2(double a00, double a01, double a10, double a11) {
    this.a00 = a00;
    this.a01 = a01;
    this.a10 = a10;
    this.a11 = a11;
  }

  /**
   * The method multiply takes in the argument vector
   * and multiplies the input parameter with this
   * matrix object.
   *
   * @param vector of the type Vector2D
   * @return a new object of the type Vector2D
   */
  public Vector2D multiply(Vector2D vector) {
    return new Vector2D(a00 * vector.getX0()
        + a01 * vector.getX1(), a10 * vector.getX0() + a11 * vector.getX1());
  }

  /**
   * The method setMatriceValues takes in the arguments
   * a00, a01, a10 and a11 and sets the values of the
   * matrix to these arguments.
   *
   * @param a00 the first element of the matrix
   * @param a01 the second element of the matrix
   * @param a10 the third element of the matrix
   * @param a11 the fourth element of the matrix
   */
  public void setMatriceValues(double a00, double a01, double a10, double a11) {
    this.a00 = a00;
    this.a01 = a01;
    this.a10 = a10;
    this.a11 = a11;
  }

  /**
   * Returns a string representation of the matrix.
   *
   * @return a string representation of the matrix
   */
  @Override
  public String toString() {
    return a00 + ", " + a01 + ", " + a10 + ", " + a11;
  }
}
