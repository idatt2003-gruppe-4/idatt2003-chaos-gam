package edu.ntnu.stud.model.math;

/**
 * This class represents a 2D vector and allows
 * mathematical operations such as subtracting
 * and addition to the vector.
 */
public class Vector2D {
  private double x0;
  private double x1;

  /**
   * Constructor for the Vector2D class.
   *
   * @param x0 the first element of the vector
   * @param x1 the second element of the vector
   */
  public Vector2D(double x0, double x1) {
    this.x0 = x0;
    this.x1 = x1;
  }

  /**
   * Returns the first element of the vector.
   *
   * @return the first element of the vector
   */
  public double getX0() {
    return x0;
  }

  /**
   * Returns the second element of the vector.
   *
   * @return the second element of the vector
   */
  public double getX1() {
    return x1;
  }

  /**
   * Allows us to add another vector on top of
   * the on in this object.
   * @param other is of type Vector2D and is the
   *              one we want to add to our object.
   *
   * @return a new object of the type Vector2D which
   * is the sum of this vector and the param other.
   */
  public Vector2D add(Vector2D other) {
    double newX0 = x0 + other.getX0();
    double newX1 = x1 + other.getX1();

    return new Vector2D(newX0, newX1);
  }

  /**
   * Allows us to subtract our vector with
   * a 2D vector.
   * @param other is of type Vector2D and is
   *              the one used to subtract from
   *              this objects vector.
   *
   * @return a new object of the type Vector2D which
   * is our vector minus the input vector.
   */
  public Vector2D subtract(Vector2D other) {
    double newX0 = x0 - other.getX0();
    double newX1 = x1 - other.getX1();

    return new Vector2D(newX0, newX1);
  }

  /**
   * Returns a string representation of the vector.
   *
   * @return a string representation of the vector
   */
  @Override
  public String toString() {
    return x0 + ", " + x1;
  }

  /**
   * Sets the first element of the vector.
   *
   * @param width
   */
  public void setX0(double width) {
    this.x0 = width;
  }

  /**
   * Sets the second element of the vector.
   *
   * @param height
   */
  public void setX1(double height) {
    this.x1 = height;
  }
}
