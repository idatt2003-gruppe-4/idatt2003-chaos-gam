package edu.ntnu.stud.model.math;

import java.lang.Math;

/**
 * Represents a complex number, extends Vector2D and
 * adds methods for calculating the
 * square root and square of a complex number.
 */
public class Complex extends Vector2D {
  public Complex(double realPart, double imaginaryPart) {
    super(realPart, imaginaryPart);

  }

  /**
   * Calculates the square root of a complex number.
   *
   * @return a new complex number
   */
  public Complex sqrt() {
    double realPart = Math.sqrt((Math.sqrt(Math.pow(
        this.getX0(), 2) + Math.pow(this.getX1(), 2)) + this.getX0()) / 2);
    double imaginaryPart = Math.signum(this.getX1()) * Math.sqrt(
        (Math.sqrt(Math.pow(this.getX0(), 2) + Math.pow(
            this.getX1(), 2)) - this.getX0()) / 2);
    return new Complex(realPart, imaginaryPart);
  }

  /**
   * Returns a string representation of the complex number.
   *
   * @return a string representation of the complex number
   */
  @Override
  public String toString() {
    return getX0() + ", " + getX1();
  }
}
