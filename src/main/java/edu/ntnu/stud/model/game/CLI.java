package edu.ntnu.stud.model.game;

import java.io.IOException;
import java.util.Scanner;

/**
 * This class represents a command line interface for the Chaos Game.
 * Contains methods for reading and writing descriptions from/to files,
 * running iterations and printing the fractal to the console.
 * The main method contains a loop that reads user input and
 * executes the  corresponding command.
 */
public class CLI {
  private ChaosGame chaosGame;
  private ChaosGameDescription description;

  /**
   * Reads a Chaos Game description from a file and
   * also initializes the ChaosGame instance.
   *
   * @param filePath the path to the file
   * @throws IOException if an error occurs while reading the file
   */

  public void readDescriptionFromFile(String filePath) throws IOException {
    description = ChaosGameFileHandler.readFromFile(filePath);;
    // Parse the description and initialize the ChaosGame instance
    chaosGame = new ChaosGame(description, 50, 50);
  }

  /**
   * Writes the description to a file.
   *
   * @param filePath the path to the file
   * @throws IOException if an error occurs while writing to the file
   */
  public void writeDescriptionToFile(String filePath) throws IOException {
    if (description != null) {
      ChaosGameFileHandler.writeToFile(description, filePath);
      System.out.println("Written to file");
    }
  }

  /**
   * Runs a given number of iterations of the Chaos Game.
   *
   * @param steps the number of iterations
   */
  public void runIterations(int steps) {
    if (chaosGame != null) {
      chaosGame.updateSteps(steps);
      chaosGame.runSteps();
    }
  }

  /**
   * Prints the ASCII representation of the fractal to the console.
   */
  public void printAsciiFractal() {
    if (chaosGame != null) {
      ChaosCanvas canvas = chaosGame.getCanvas();
      // Convert the canvas to ASCII and print it
      System.out.println(canvas.toString());
    }
  }

  /**
   * Main method that reads user input and executes the corresponding
   * command.
   *
   * @param args the command line arguments
   */
  public static void main(String[] args) {
    Scanner scanner = new Scanner(System.in);
    CLI cli = new CLI();
    boolean isRunning = true;

    while (isRunning) {
      System.out.println("Please select an option:");
      System.out.println("1. Read description from file");
      System.out.println("2. Write description to file");
      System.out.println("3. Run a given number of iterations");
      System.out.println("4. Print ASCII fractal to console");
      System.out.println("5. Exit");

      String input = scanner.nextLine();

      try {
        switch (input) {
          case "1":
            cli.readDescriptionFromFile("src/main/resources/Triangle.txt");
            break;
          case "2":
            System.out.println("Input file path: ");
            input = scanner.nextLine();
            cli.writeDescriptionToFile(input);
            break;
          case "3":
            System.out.println("How many iterations: ");
            int steps = scanner.nextInt();
            scanner.nextLine();
            cli.runIterations(steps);
            break;
          case "4":
            cli.printAsciiFractal();
            break;
          case "5":
            isRunning = false;
            break;
          default:
            System.out.println("Invalid option. Please try again.");
            break;
        }
      } catch (IOException e) {
        System.out.println("An error occurred: " + e.getMessage());
      }
    }

    scanner.close();
  }
}