package edu.ntnu.stud.model.game;

import edu.ntnu.stud.model.math.Matrix2x2;
import edu.ntnu.stud.model.math.Vector2D;
import edu.ntnu.stud.model.transformation.AffineTransform2D;

/**
 * This class represents a canvas for the Chaos Game.
 */
public class ChaosCanvas {
  private int[][] canvas;
  private int width;
  private int height;
  private AffineTransform2D transformCoordsToIndices;
  private Vector2D minCoords;
  private Vector2D maxCoords;

  /**
   * Constructor for the ChaosCanvas class.
   *
   * @param width the width of the canvas
   * @param height the height of the canvas
   * @param minCoords the minimum coordinates of the canvas
   * @param maxCoords the maximum coordinates of the canvas
   */
  public ChaosCanvas(int width, int height, Vector2D minCoords, Vector2D maxCoords) {
    this.width = width;
    this.height = height;
    this.canvas = new int[height][width];
    this.minCoords = minCoords;
    this.maxCoords = maxCoords;
  }

  /**
   * Returns the width of the canvas.
   *
   * @return the width of the canvas
   */
  public AffineTransform2D getTransformCoordsToIndices() {
    this.transformCoordsToIndices = new AffineTransform2D(
        new Matrix2x2(0, (height - 1) / (minCoords.getX1() - maxCoords.getX1()),
            (width - 1) / (maxCoords.getX0() - minCoords.getX0()), 0),

        new Vector2D(((height - 1) *  maxCoords.getX1()) / (maxCoords.getX1() - minCoords.getX1()),
            ((width - 1) * minCoords.getX0()) / (minCoords.getX0() - maxCoords.getX0()))
    );
    return this.transformCoordsToIndices;
  }

  /**
   * Gets the value of a pixel in a specified location.
   *
   * @param point a point with x and y coordinates
   * @return integer, either 1 or 0
   */
  public int getPixel(Vector2D point) {
    Vector2D pixel = getTransformCoordsToIndices().transform(point);
    return canvas[(int) pixel.getX1()][(int) pixel.getX0()];
  }

  /**
   * Sets the value of a pixel, 1.
   *
   * @param point a point with x and y coordinates
   */
  public void putPixel(Vector2D point) {
    Vector2D canvasPoint = getTransformCoordsToIndices().transform(point);
    int x = (int) Math.round(canvasPoint.getX0());
    int y = (int) Math.round(canvasPoint.getX1());

    // Check if the rounded coordinates fall within the bounds of the canvas
    if (x >= 0 && x < canvas.length && y >= 0 && y < canvas[0].length) {
      canvas[x][y] = 1;
    }
  }

  /**
   * Returns the 2D array of the canvas.
   *
   * @return 2D array of the canvas
   */
  public int[][] getCanvasArray() {
    return canvas;
  }

  /**
   * Clears the canvas by setting all the cells to 0.
   */
  public void clear() {
    for (int i = 0; i < canvas.length; i++) {
      for (int j = 0; j < canvas[i].length; j++) {
        canvas[i][j] = 0;
      }
    }
  }

  /**
   * Sets the minimum coordinates of the canvas.
   *
   * @return the minimum coordinates of the canvas
   */
  public void setMinCoords(Vector2D minCoords) {
    this.minCoords = minCoords;
  }

  /**
   * Sets  the maximum coordinates of the canvas.
   *
   * @return the maximum coordinates of the canvas
   */
  public void setMaxCoords(Vector2D maxCoords) {
    this.maxCoords = maxCoords;
  }

  /**
   *  Returns a string representation of the canvas.
   * @return a string representation of the canvas
   */
  @Override
  public String toString() {
    String output = "";
    for (int i = 0; i < width; i++) {
      for (int j = 0; j < height; j++) {
        if (canvas[i][j] == 0) {
          output += " ";
        } else {
          output += "X";
        }
      }
      output += "\n";
    }
    return output;
  }
}
