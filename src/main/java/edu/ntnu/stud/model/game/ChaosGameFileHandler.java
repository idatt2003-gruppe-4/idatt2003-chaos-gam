package edu.ntnu.stud.model.game;

import edu.ntnu.stud.model.math.Complex;
import edu.ntnu.stud.model.math.Matrix2x2;
import edu.ntnu.stud.model.math.Vector2D;
import edu.ntnu.stud.model.transformation.AffineTransform2D;
import edu.ntnu.stud.model.transformation.JuliaTransform;
import edu.ntnu.stud.model.transformation.Transform2D;
import java.io.*;
import java.util.*;

/**
 * Handles file operations for Chaos Game descriptions.
 */
public class ChaosGameFileHandler {
  /**
   * Removes comments from lines in a text file.
   *
   * @param line a string containing one line of data
   * @return new line string without its comment
   */
  public static String removeComments(String line) {
    if (line.contains("#")) {
      return line.split("#")[0].trim();
    } else {
      return line;
    }
  }


  /**
   * This method reads a Chaos Game description from a file.
   * The file should contain the type of transformation, the coordinates
   * for the lower left and upper right corners,
   * and the parameters for the transformations.
   * The transformations are added to an ArrayList and used to create
   * a ChaosGameDescription.
   *
   * @param fileName the name of the file to read from
   * @return  ChaosGameDescription read from the file
   * @throws FileNotFoundException if the file is not found
   */
  public static ChaosGameDescription readFromFile(
      String fileName) throws FileNotFoundException {
    try (Scanner scanner = new Scanner(new File(fileName))) {
      scanner.useLocale(Locale.ENGLISH);
      String type = removeComments(scanner.nextLine());

      String newLine = removeComments(scanner.nextLine());
      Vector2D minCords = new Vector2D(Double.parseDouble(
          newLine.split(",")[0]), Double.parseDouble(
              newLine.split(",")[1])); // Read coordinates for lower left corner

      newLine = removeComments(scanner.nextLine());
      Vector2D maxCords = new Vector2D(Double.parseDouble(newLine.split(
          ",")[0]), Double.parseDouble(
              newLine.split(",")[1])); // Read coordinates for upper right corner


      ArrayList<Transform2D> transforms = new ArrayList<>();


      while (scanner.hasNextLine()) {
        newLine = removeComments(scanner.nextLine());
        if (type.equals("Affine2D")) {
          double[] transformation = new double[6];
          for (int i = 0; i < 6; i++) {
            transformation[i] = Double.parseDouble(newLine.split(",")[i]);
          }

          transforms.add(new AffineTransform2D(new Matrix2x2(
              transformation[0], transformation[1], transformation[2],
              transformation[3]),
              new Vector2D(transformation[4], transformation[5])));
        } else if (type.equals("Julia")) {
          double real = Double.parseDouble(newLine.split(",")[0]);
          double imaginary = Double.parseDouble(newLine.split(",")[1]);
          Complex complex = new Complex(real, imaginary);
          transforms.add(new JuliaTransform(complex, 1));
          transforms.add(new JuliaTransform(complex, -1));
        }
      }
      scanner.close();
      return new ChaosGameDescription(transforms, minCords, maxCords);
    }
  }

  /**
   * This method writes a ChaosGameDescription to a text file.
   * The file is named "output.txt" and is placed in the specified filepath.
   * The ChaosGameDescription is converted to a string and written to the file.
   *
   * @param description the data for the chaos game
   * @param filepath the path to where the text file is to be placed
   * @throws FileNotFoundException if the file is not found
   */
  public static void writeToFile(ChaosGameDescription description, String filepath) {
    try {
      File fileOutput;

      if (description.getTransforms().get(0) instanceof AffineTransform2D) {
        fileOutput = new File(filepath + "/output.txt");
      } else if (description.getTransforms().get(0) instanceof JuliaTransform) {
        fileOutput = new File(filepath + "/output.txt");
      } else {
        throw new IllegalArgumentException("Unknown transformation type");
      }

      PrintWriter writer = new PrintWriter(fileOutput);
      writer.write(description.toString());
      writer.close();
    } catch (FileNotFoundException e) {
      System.out.println("File could not be found");
    }
  }
}
