package edu.ntnu.stud.model.game;

import edu.ntnu.stud.model.ChaosGameObserver;
import edu.ntnu.stud.model.ChaosGameSubject;
import edu.ntnu.stud.model.math.Complex;
import edu.ntnu.stud.model.math.Matrix2x2;
import edu.ntnu.stud.model.math.Vector2D;
import edu.ntnu.stud.model.transformation.AffineTransform2D;
import edu.ntnu.stud.model.transformation.JuliaTransform;
import edu.ntnu.stud.model.transformation.Transform2D;
import java.util.ArrayList;
import java.util.Random;

/**
 * The ChaosGame  implements the ChaosGameSubject
 * interface and is responsible for running the  chaos game.
 *  Contains the canvas and the description of the game.
 *  Also contains the current point and the number of steps.
 *  Contains a list of observers as well.
 */
public class ChaosGame implements ChaosGameSubject {
  private ChaosCanvas canvas;
  private ChaosGameDescription description;
  private Vector2D currentPoint = new Vector2D(0, 0);
  public final Random random;
  public ArrayList<ChaosGameObserver> observers = new ArrayList<>();
  public int steps = 1500;
  public Transform2D transform;

  /**
   * Constructor for the ChaosGame class.
   *
   * @param description the description of the chaos game
   * @param width the width of the canvas
   * @param height the height of the canvas
   */
  public ChaosGame(ChaosGameDescription description, int width, int height) {
    if (description == null) {
      throw new IllegalArgumentException("Description cannot be null");
    }
    if (width <= 0 || height <= 0) {
      throw new IllegalArgumentException("Width and height must be positive");
    }
    this.description = description;
    this.random = new Random();
    this.canvas = new ChaosCanvas(
        width, height, description.getMinCords(), description.getMaxCords());
  }

  /**
   * Returns the canvas.
   *
   * @return canvas of the type ChaosCanvas
   */
  public ChaosCanvas getCanvas() {
    return canvas;
  }

  /**
   * Runs the chaos game for the specified number of steps.
   */
  public void runSteps() {
    if (steps < 0) {
      throw new IllegalArgumentException("Number of steps cannot be negative");
    }
    for (int i = 0; i < steps; i++) {
      transform = description.getTransforms()
          .get(random.nextInt(0, description.getTransforms().size()));

      Vector2D tmp = transform.transform(currentPoint);
      currentPoint.setX0(tmp.getX0());
      currentPoint.setX1(tmp.getX1());


      canvas.putPixel(currentPoint);
    }
  }

  /**
   * Returns the transformation used in the Chaos Game.
   *
   * @return the current point
   */
  public Transform2D getTransform() {
    return transform;
  }

  /**
   * Updates ChaosGame with a new description.
   * @param description the new description.
   */
  public void updateChaosGame(ChaosGameDescription description) {
    if (description == null) {
      throw new IllegalArgumentException("Description cannot be null");
    }
    this.description = description;
    this.canvas.setMinCoords(description.getMinCords());
    this.canvas.setMaxCoords(description.getMaxCords());
    notifyObservers();
  }

  /**
   * Updates the current point.
   *
   * @param x0 the x-coordinate of the point
   * @param x1 the y-coordinate of the point
   */
  public void updateMaxCoords(double x0, double x1) {
    this.description.setMaxCords(x0, x1);
    notifyObservers();
  }

  /**
   * Updates the current point.
   *
   * @param x0 the x-coordinate of the point
   * @param x1 the y-coordinate of the point
   */
  public void updateMinCoords(double x0, double x1) {
    description.setMinCords(x0, x1);
    notifyObservers();
  }


  /**
  * Updates the number of steps.
   *
   * @param steps the new number of steps
   */
  public void updateSteps(int steps) {
    if (steps > 0) {
      this.steps = steps;
      notifyObservers();
    } else {
      throw new IllegalArgumentException("Number of steps cannot be negative");
    }
  }

  /**
   * Returns the description of the ChaosGame.
   *
   * @return the description of the ChaosGame
   */
  public ChaosGameDescription getDescription() {
    return this.description;
  }

  /**
   * Updates the matrix and vector of the transformation.
   *
   * @param index the index of the transformation
   * @param A the matrix
   * @param b the vector
   */
  public void updateMatrixAndVector(int index, Matrix2x2 A, Vector2D b) {
    ArrayList<Transform2D> transform2DS = description.getTransforms();
    transform2DS.set(index, new AffineTransform2D(A, b));
    notifyObservers();
  }

  /**
   * Updates the complex constant of the Julia transformation.
   *
   * @param c the complex constant
   */
  public void updateComplexConstantJulia(Complex c) {
    ArrayList<Transform2D> transform2DS = description.getTransforms();
    transform2DS.set(0, new JuliaTransform(c, 1));
    transform2DS.set(1, new JuliaTransform(c, -1));
    notifyObservers();
  }

  /**
   * Attaches an observer to the list of observers.
   *
   * @param o the observer to be attached
   */
  @Override
  public void attach(ChaosGameObserver o) {
    observers.add(o);
  }

  /**
   * Detaches an observer from the list of observers.
   *
   * @param o the observer to be detached
   */
  @Override
  public void dettach(ChaosGameObserver o) {
    observers.remove(o);
  }

  /**
   * Notifies all observers in the list of observers.
   */
  @Override
  public void notifyObservers() {
    for (ChaosGameObserver observer : observers) {
      observer.update();
    }
  }
}