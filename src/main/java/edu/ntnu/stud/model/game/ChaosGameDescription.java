package edu.ntnu.stud.model.game;

import edu.ntnu.stud.model.math.Vector2D;
import edu.ntnu.stud.model.transformation.AffineTransform2D;
import edu.ntnu.stud.model.transformation.JuliaTransform;
import edu.ntnu.stud.model.transformation.Transform2D;
import java.util.ArrayList;

/**
 * This class represents the description of the Chaos Game.
 * Contains the transforms, the minimum and maximum coordinates.
 */
public class ChaosGameDescription {
  private Vector2D minCords;
  private Vector2D maxCords;
  private final ArrayList<Transform2D> transforms;

  /**
   * Constructor for the ChaosGameDescription class.
   *
   * @param transforms the transforms used in the chaos game
   * @param minCords the minimum coordinates of the canvas
   * @param maxCords the maximum coordinates of the canvas
   */
  public ChaosGameDescription(
      ArrayList<Transform2D> transforms, Vector2D minCords, Vector2D maxCords) {
    if (transforms == null) {
      throw new IllegalArgumentException("Transforms cannot be null");
    }
    if (minCords == null) {
      throw new IllegalArgumentException("MinCords cannot be null");
    }
    if (maxCords == null) {
      throw new IllegalArgumentException("MaxCords cannot be null");
    }
    this.transforms = transforms;
    this.minCords = minCords;
    this.maxCords = maxCords;
  }

  /**
   * Returns the transformation.
   *
   * @return ArrayList of type Transform2D which is the transform
   */
  public ArrayList<Transform2D> getTransforms() {
    return this.transforms;
  }

  /**
   * Returns the min coordinates.
   *
   * @return Vector2D min coordinates
   */
  public Vector2D getMinCords() {
    return this.minCords;
  }

  /**
   * Returns the max coordinates.
   *
   * @return Vector2D max coordinates.
   */
  public Vector2D getMaxCords() {
    return this.maxCords;
  }

  /**
   * Sets the min coordinates.
   *
   * @param x0
   * @param x1
   */
  public void setMinCords(double x0, double x1) {
    this.minCords.setX0(x0);
    this.minCords.setX1(x1);
  }

  /**
   * Sets the max coordinates.
   *
   * @param x0
   * @param x1
   */
  public void setMaxCords(double x0, double x1) {
    this.maxCords.setX0(x0);
    this.maxCords.setX1(x1);
  }

  /**
   * Returns a string representation of the ChaosGameDescription.
   *
   * @return a string representation of the ChaosGameDescription
   */
  @Override
  public String toString() {
    StringBuilder output = new StringBuilder();

    if (!transforms.isEmpty()) {
      if (transforms.get(0) instanceof JuliaTransform) {
        output.append("Julia");
      } else if (transforms.get(0) instanceof AffineTransform2D) {
        output.append("Affine2D");
      }
      output.append("    # Type of transform\n");
    }

    output.append(minCords).append("    # Lower left\n");
    output.append(maxCords).append("    # Upper right\n");
    for (int i = 0; i < transforms.size(); i++) {
      if (i == transforms.size() - 1 && transforms.get(i) instanceof JuliaTransform) {
        continue;
      }
      output.append(transforms.get(i)).append("    # transform\n");
    }
    return output.toString();
  }
}