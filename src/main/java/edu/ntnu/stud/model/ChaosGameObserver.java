package edu.ntnu.stud.model;

/**
 * Represents a ChaosGameObserver interface.
 * Contains a method for updating the observer.
 */
public interface ChaosGameObserver {
  void update();
}
