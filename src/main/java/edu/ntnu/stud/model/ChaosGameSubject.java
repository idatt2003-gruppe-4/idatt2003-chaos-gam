package edu.ntnu.stud.model;

/**
 * Represents a ChaosGameSubject interface.
 * Contains methods for attaching, detaching and notifying observers.
 */
public interface ChaosGameSubject {

  /**
   * Notifies all observers.
   */
  void notifyObservers();

  /**
   * Attaches an observer.
   *
   * @param o the observer to attach
   */
  void attach(ChaosGameObserver o);

  /**
   * Detaches an observer.
   *
   * @param o the observer to detach
   */
  void dettach(ChaosGameObserver o);
}
