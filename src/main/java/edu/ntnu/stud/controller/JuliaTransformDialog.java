package edu.ntnu.stud.controller;

import edu.ntnu.stud.model.game.ChaosGame;
import edu.ntnu.stud.model.game.ChaosGameDescription;
import edu.ntnu.stud.model.game.ChaosGameFileHandler;
import edu.ntnu.stud.model.math.Complex;
import edu.ntnu.stud.model.math.Vector2D;
import edu.ntnu.stud.model.transformation.JuliaTransform;
import edu.ntnu.stud.model.transformation.Transform2D;
import edu.ntnu.stud.view.components.JuliaParameterFields;
import java.util.ArrayList;
import javafx.scene.control.ButtonBar;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Dialog;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.util.Pair;

/**
 * The {@code JuliaTransformDialog} class is responsible for creating a
 * dialog frame that allows the user to input parameters for the julia transformation.
 * The user is tasked with answering what values the
 * imaginary and real part should be, as well as
 * the minimum and maximum coordinates.
 * The collected data is used to update a {@link ChaosGame}
 * instance and save the configuration to a file.
 */
public class JuliaTransformDialog {

  /**
   * Constructs an {@code JuliaTransformDialog} for the specified {@link ChaosGame}.
   * It prompts the user to fill in the values for real number, imaginary number,
   * minimum coordinates and maximum coordinates.
   * Lastly, {@link ChaosGame} is updated with the new configuration and saves it to a file.
   *
   * @param chaosGame the {@link ChaosGame} instance to be updated with the new transformations.
   */
  public JuliaTransformDialog(ChaosGame chaosGame) {
    Dialog<Pair<String, String>> dialog = new Dialog<>();
    dialog.setTitle("Julia Transformation Parameters");
    dialog.setHeaderText("Enter the parameters for the Julia transformation:");

    ButtonType okButtonType = new ButtonType("OK", ButtonBar.ButtonData.OK_DONE);
    dialog.getDialogPane().getButtonTypes().addAll(okButtonType, ButtonType.CANCEL);

    GridPane grid = JuliaParameterFields.juliaParameters();

    dialog.getDialogPane().setContent(grid);

    ArrayList<TextField> allFields = new ArrayList<>();
    for (int i = 1; i < grid.getChildren().size(); i += 2) {
      allFields.add((TextField) grid.getChildren().get(i));
    }

    // Add a listener to each text field to enable/disable the OK button
    allFields.forEach(field -> field.textProperty().addListener(
        (observable, oldValue, newValue) -> {
        boolean allFilled = allFields.stream().noneMatch(f -> f.getText().trim().isEmpty());
        dialog.getDialogPane().lookupButton(okButtonType).setDisable(!allFilled);
      }));

    dialog.getDialogPane().lookupButton(okButtonType).setDisable(true);
    dialog.setResultConverter(dialogButton -> {
      if (dialogButton == okButtonType) {

        double real = Double.parseDouble(((TextField) grid.getChildren().get(1)).getText());
        double imaginary = Double.parseDouble(((TextField) grid.getChildren().get(3)).getText());
        double min1 = Double.parseDouble(((TextField) grid.getChildren().get(5)).getText());
        double min2 = Double.parseDouble(((TextField) grid.getChildren().get(7)).getText());
        double max1 = Double.parseDouble(((TextField) grid.getChildren().get(9)).getText());
        double max2 = Double.parseDouble(((TextField) grid.getChildren().get(11)).getText());

        ArrayList<Transform2D> transforms = new ArrayList<>();
        Complex complexC = new Complex(real, imaginary);
        transforms.add(new JuliaTransform(complexC, 1));
        transforms.add(new JuliaTransform(complexC, -1));


        ChaosGameDescription description = new ChaosGameDescription(
            transforms, new Vector2D(min1, min2), new Vector2D(max1, max2));


        ChaosGameFileHandler.writeToFile(description, "src/main/resources");


        chaosGame.updateChaosGame(description);
      }
      return null;
    });
    dialog.showAndWait();
  }
}