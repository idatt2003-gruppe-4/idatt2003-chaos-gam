package edu.ntnu.stud.controller;

import java.util.function.UnaryOperator;
import javafx.scene.control.TextFormatter;

/**
 * This class provides methods to format the input in a specific way.
 */
public class InputFormatter {

  /**
   * This method returns a UnaryOperator that acts as a filter for
   * multiple inputs.
   * The filter allows a string that represents a sequence of numbers
   * separated by commas.
   * Each number can be a decimal and can be negative.
   * If the new text does not match the pattern, the change is not applied.
   *
   * @return a UnaryOperator acting as a filter for multiple inputs
   */
  public static UnaryOperator<TextFormatter.Change> multipleInputsFilter() {
    return change -> {
      String newText = change.getControlNewText();
      if (newText.matches("-?\\d*(\\.\\d*)?(,\\s*-?\\d*(\\.\\d*)?)*")) {
        return change;
      }
      return null;
    };
  }

  /**
   * This method returns a UnaryOperator that acts as a
   * filter for numerical inputs.
   * The filter allows a string that represents a number.
   * The number can be a decimal and can be negative.
   * If the new text does not match the pattern, the change
   * is not applied.
   *
   * @return a UnaryOperator acting as a filter for numerical inputs
   */
  public static UnaryOperator<TextFormatter.Change> numericalFilter() {
    return change -> {
      String newText = change.getControlNewText();
      if (newText.matches("-?\\d*(\\.\\d*)?")) {
        return change;
      }
      return null;
    };
  }
}