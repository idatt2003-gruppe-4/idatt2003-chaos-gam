package edu.ntnu.stud.controller;

import edu.ntnu.stud.model.game.ChaosGame;
import edu.ntnu.stud.model.math.Complex;
import edu.ntnu.stud.model.math.Matrix2x2;
import edu.ntnu.stud.model.math.Vector2D;
import edu.ntnu.stud.model.transformation.AffineTransform2D;
import edu.ntnu.stud.model.transformation.JuliaTransform;
import edu.ntnu.stud.model.transformation.Transform2D;
import edu.ntnu.stud.view.components.JuliaParameterFields;
import java.util.ArrayList;
import javafx.scene.control.*;
import javafx.scene.layout.GridPane;
import javafx.util.Pair;

/**
 * The {@code ParameterDialog} class is responsible for displaying dialogs to the user
 * to input parameters for different types of transformations,
 * ({@link AffineTransform2D} and {@link JuliaTransform}).
 * It validates user input and updates the {@link ChaosGame} instance with the new parameters.
 */
public class ParameterDialog {
  private final ChaosGame chaosGame;
  private final Alert alert = new Alert(Alert.AlertType.ERROR);

  /**
   * Constructs a {@code ParameterDialog} for the specified {@link ChaosGame}.
   * Initializes an error alert for handling validation errors.
   *
   * @param chaosGame the {@link ChaosGame} instance to be updated with the new parameters.
   */
  public ParameterDialog(ChaosGame chaosGame) {
    this.chaosGame = chaosGame;
    alert.setTitle("Error");
    alert.setHeaderText("Validation Error");
  }

  /**
   * Handles the dialog display for a given {@link Transform2D} type.
   * Depending on the type of transformation, it either shows the Julia transformation dialog
   * or the Affine transformation dialog.
   *
   * @param transform the {@link Transform2D} instance to be handled.
   */
  public void handleTransform(Transform2D transform) {
    if (transform instanceof JuliaTransform) {
      showJuliaDialog();
    } else if (transform instanceof AffineTransform2D) {
      showAffineTransformDialog();
    }
  }

  /**
   * Displays a dialog to input parameters for a {@link JuliaTransform}.
   * Validates and updates the {@link ChaosGame} instance with the new parameters.
   */
  private void showJuliaDialog() {
    // Create the custom dialog.
    Dialog<Pair<String, String>> dialog = new Dialog<>();
    dialog.setTitle("Julia Transformation Parameters");
    dialog.setHeaderText("Enter the parameters for the Julia transformation:");

    // Set the button types.
    ButtonType okButtonType = new ButtonType("OK", ButtonBar.ButtonData.OK_DONE);
    dialog.getDialogPane().getButtonTypes().addAll(okButtonType, ButtonType.CANCEL);

    GridPane grid = JuliaParameterFields.juliaParameters();

    dialog.getDialogPane().setContent(grid);

    // Convert the result to a pair when the OK button is clicked
    dialog.setResultConverter(dialogButton -> {
      if (dialogButton == okButtonType) {
        String real = ((TextField) grid.getChildren().get(1)).getText();
        String imaginary = ((TextField) grid.getChildren().get(3)).getText();
        String min1 = ((TextField) grid.getChildren().get(5)).getText();
        String min2 = ((TextField) grid.getChildren().get(7)).getText();
        String max1 = ((TextField) grid.getChildren().get(9)).getText();
        String max2 = ((TextField) grid.getChildren().get(11)).getText();

        if (!real.isBlank() && !imaginary.isBlank()) {
          Complex complexC = new Complex(Double.parseDouble(real), Double.parseDouble(imaginary));
          chaosGame.updateComplexConstantJulia(complexC);
        }
        checkPairs(real, imaginary, "Real number", "Imaginary number");

        checkCoordinates(min1, min2, max1, max2);
      }
      return null;
    });

    dialog.showAndWait();
  }

  /**
   * Displays a dialog to input parameters for an {@link AffineTransform2D}.
   * Validates and updates the {@link ChaosGame} instance with the new parameters.
   */
  private void showAffineTransformDialog() {

    // Create the custom dialog.
    Dialog<Pair<String, String>> dialog = new Dialog<>();
    dialog.setTitle("Affine Transformation Parameters");
    dialog.setHeaderText("Enter the parameters for the Affine transformation:");

    // Set the button types.
    ButtonType okButtonType = new ButtonType("OK", ButtonBar.ButtonData.OK_DONE);
    dialog.getDialogPane().getButtonTypes().addAll(okButtonType, ButtonType.CANCEL);

    // Layout the fields in a grid pane
    GridPane grid = new GridPane();

    int row = 0;
    int numTransformations = chaosGame.getDescription().getTransforms().size();

    ArrayList<TextField> matrixFields = new ArrayList<>();
    ArrayList<TextField> vectorFields = new ArrayList<>();

    for (int i = 0; i < numTransformations; i++) {
      grid.add(new javafx.scene.control.Label("Matrix A" + (i + 1) + ": "), 0, row);
      TextField matrixField = new TextField();
      matrixField.setPromptText("a00, a01, a10, a11");
      matrixField.setTextFormatter(new TextFormatter<>(InputFormatter.multipleInputsFilter()));
      grid.add(matrixField, 1, row++);
      matrixFields.add(matrixField);

      grid.add(new javafx.scene.control.Label("Vector b" + (i + 1) + ": "), 0, row);
      TextField vectorField = new TextField();
      vectorField.setPromptText("b0, b1");
      vectorField.setTextFormatter(new TextFormatter<>(InputFormatter.multipleInputsFilter()));
      grid.add(vectorField, 1, row++);
      vectorFields.add(vectorField);
    }

    TextField minCoords1 = new TextField();
    minCoords1.setTextFormatter(new TextFormatter<>(InputFormatter.numericalFilter()));
    minCoords1.setPromptText("Min coordinates 1");
    TextField minCoords2 = new TextField();
    minCoords2.setTextFormatter(new TextFormatter<>(InputFormatter.numericalFilter()));
    minCoords2.setPromptText("Min coordinates 2");
    TextField maxCoords1 = new TextField();
    maxCoords1.setTextFormatter(new TextFormatter<>(InputFormatter.numericalFilter()));
    maxCoords1.setPromptText("Max coordinates 1");
    TextField maxCoords2 = new TextField();
    maxCoords2.setTextFormatter(new TextFormatter<>(InputFormatter.numericalFilter()));
    maxCoords2.setPromptText("Max coordinates 2");

    grid.add(new Label("Min coordinates 1: "), 0, row);
    grid.add(minCoords1,  1, row++);
    grid.add(new Label("Min coordinates 2: "), 0, row);
    grid.add(minCoords2, 1, row++);
    grid.add(new Label("Max coordinates 1: "), 0, row);
    grid.add(maxCoords1, 1,  row++);
    grid.add(new Label("Max coordinates 2: "), 0, row);
    grid.add(maxCoords2, 1,  row);


    dialog.getDialogPane().setContent(grid);

    // Convert the result to a pair when the OK button is clicked
    dialog.setResultConverter(dialogButton -> {
      if (dialogButton == okButtonType) {
        // Here you can handle the input values
        for (int i = 0; i < numTransformations; i++) {
          String matrixText = matrixFields.get(i).getText();
          String vectorText = vectorFields.get(i).getText();


          String[] matrixValues = matrixText.split(",");
          String[] vectorValues = vectorText.split(",");

          if (matrixValues.length == 4 && vectorValues.length == 2) {
            // Parse matrix values
            ArrayList<Double> matrixArray = new ArrayList<>();
            for (String matrixValue : matrixValues) {
              try {
                matrixArray.add(Double.parseDouble(matrixValue));
              } catch (Exception e) {
                alert.setContentText(
                    "Input could not be handled, be sure to not "
                        + "write inputs with multiple dots."
                );
                alert.showAndWait();
              }
            }

            // Parse vector values
            ArrayList<Double> vectorArray = new ArrayList<>();
            for (String vectorValue : vectorValues) {
              try {
                vectorArray.add(Double.parseDouble(vectorValue));
              } catch (Exception e) {
                alert.setContentText(
                    "Input could not be handled, be sure to not "
                        + "write inputs with multiple dots."
                );
                alert.showAndWait();
              }
            }

            try {
              Matrix2x2 matrixA = new Matrix2x2(
                  matrixArray.get(0), matrixArray.get(1), matrixArray.get(2), matrixArray.get(3));
              Vector2D b = new Vector2D(vectorArray.get(0), vectorArray.get(1));
              chaosGame.updateMatrixAndVector(i, matrixA, b);
            } catch (Exception ignored) {
            }

          } else if (matrixText.isBlank() && vectorText.isBlank()) {
          } else {
            alert.setContentText(
                "Both the matrix " + (i + 1) + " and vector " + (i + 1)
                    + " has to be filled and follow "
                    + "a00, a01, a10, a11 and b0, b1."
            );
            alert.showAndWait();
          }
        }

        String min1 = minCoords1.getText();
        String min2 = minCoords2.getText();
        String max1 = maxCoords1.getText();
        String max2 = maxCoords2.getText();

        checkCoordinates(min1, min2, max1, max2);
      }
      return null;
    });
    dialog.showAndWait();
  }

  /**
   * Checks and updates the minimum and maximum coordinates of the {@link ChaosGame} instance.
   * Displays an error alert if the inputs are invalid.
   *
   * @param min1 the minimum coordinate value in the first dimension.
   * @param min2 the minimum coordinate value in the second dimension.
   * @param max1 the maximum coordinate value in the first dimension.
   * @param max2 the maximum coordinate value in the second dimension.
   */
  private void checkCoordinates(String min1, String min2, String max1, String max2) {
    if (!min1.isBlank() && !min2.isBlank()) {
      chaosGame.updateMinCoords(Double.parseDouble(min1), Double.parseDouble(min2));
    }
    checkPairs(min1, min2, "Min coordinates 1", "Min coordinates 2");

    if (!max1.isBlank() && !max2.isBlank()) {
      chaosGame.updateMaxCoords(Double.parseDouble(max1), Double.parseDouble(max2));
    }
    checkPairs(max1, max2, "Max coordinates 1", "Max coordinates 2");
  }

  /**
   * Checks if a pair of input strings are both filled or both empty.
   * Displays an error alert if only one of the strings is filled.
   *
   * @param string1       the first string to be checked.
   * @param string2       the second string to be checked.
   * @param variableName1 the name of the first variable for the error message.
   * @param variableName2 the name of the second variable for the error message.
   */
  private void checkPairs(String string1, String string2,
                          String variableName1, String variableName2) {
    if ((string1.isBlank() && !string2.isBlank()) || (!string1.isBlank() && string2.isBlank())) {
      alert.setContentText("Both " + variableName1 + " and " + variableName2 + " need to filled.");
      alert.showAndWait();
    }
  }
}