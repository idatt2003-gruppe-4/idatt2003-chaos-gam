package edu.ntnu.stud.controller;

import edu.ntnu.stud.view.components.CanvasViewComponent;
import java.io.FileNotFoundException;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;

/**
 * The {@code ChaosGameDescriptionFactory} class hold three predefined
 * filepaths to three different fractals. They will update
 * {@link CanvasViewComponent} filepath value.
 */
public class ChaosGameDescriptionFactory {

  /**
   * Event handler which will call the {@link CanvasViewComponent}
   * updateFile method which sets the new filepath to be the Julia.txt
   */
  public static EventHandler<ActionEvent> juliaListener = event ->  {
    try {
      CanvasViewComponent.updateFile("src/main/resources/Julia.txt");
    } catch (FileNotFoundException e) {
      throw new RuntimeException(e);
    }
  };

  /**
   * Event handler which will call the {@link CanvasViewComponent}
   * updateFile method which sets the new filepath to be the Barnsley.txt
   */
  public static EventHandler<ActionEvent> barnsleyListener = event -> {
    try {
      CanvasViewComponent.updateFile("src/main/resources/Barnsley.txt");
    } catch (FileNotFoundException e) {
      throw new RuntimeException(e);
    }
  };

  /**
   * Event handler which will call the {@link CanvasViewComponent}
   * updateFile method which sets the new filepath to be the Triangle.txt
   */
  public static EventHandler<ActionEvent> triangleListener = event -> {
    try {
      CanvasViewComponent.updateFile("src/main/resources/Triangle.txt");
    } catch (FileNotFoundException e) {
      throw new RuntimeException(e);
    }
  };
}
