package edu.ntnu.stud.controller;

import edu.ntnu.stud.model.game.ChaosGame;
import edu.ntnu.stud.model.game.ChaosGameDescription;
import edu.ntnu.stud.model.game.ChaosGameFileHandler;
import edu.ntnu.stud.model.math.Matrix2x2;
import edu.ntnu.stud.model.math.Vector2D;
import edu.ntnu.stud.model.transformation.AffineTransform2D;
import edu.ntnu.stud.model.transformation.Transform2D;
import java.util.ArrayList;
import javafx.scene.control.*;
import javafx.scene.layout.GridPane;

/**
 * The {@code AffineTransformDialog} class is responsible for creating a
 * dialog frame that allows the user to input parameters for the affine transformation.
 * The user is tasked with answering how many transformations,
 * their corresponding matrix and vector values, and the minimum and maximum coordinates.
 * The collected data is used to update a {@link ChaosGame}
 * instance and save the configuration to a file.
 */
public class AffineTransformDialog {

  /**
   * Constructs an {@code AffineTransformDialog} for the specified {@link ChaosGame}.
   * It first prompts the user to select the number of transformations,
   * then collects the parameters for each affine transformation.
   * Lastly, {@link ChaosGame} is updated with the new configuration and saves it to a file.
   *
   * @param chaosGame the {@link ChaosGame} instance to be updated with the new transformations.
   */
  public AffineTransformDialog(ChaosGame chaosGame) {
    Dialog<Integer> initialDialog = new Dialog<>();
    initialDialog.setTitle("Number of Transformations");
    initialDialog.setHeaderText("Select the number of transformations:");

    ButtonType okButtonType = new ButtonType("OK", ButtonBar.ButtonData.OK_DONE);
    initialDialog.getDialogPane().getButtonTypes().addAll(okButtonType, ButtonType.CANCEL);

    ComboBox<Integer> numberOfTransformationsComboBox = new ComboBox<>();
    numberOfTransformationsComboBox.getItems().addAll(1, 2, 3, 4, 5, 6);
    numberOfTransformationsComboBox.setValue(1); // Default value

    GridPane initialGrid = new GridPane();
    initialGrid.add(numberOfTransformationsComboBox, 0, 0);

    initialDialog.getDialogPane().setContent(initialGrid);
    initialDialog.setResultConverter(dialogButton -> {
      if (dialogButton == okButtonType) {
        return numberOfTransformationsComboBox.getValue();
      }
      return null;
    });

    // Wait for the user to select the number of transformations
    initialDialog.showAndWait().ifPresent(numTransformations -> {

      Dialog<Void> dialog = new Dialog<>();
      dialog.setTitle("Affine Transformation Parameters");
      dialog.setHeaderText("Enter the parameters for the affine transformation:");

      dialog.getDialogPane().getButtonTypes().addAll(okButtonType, ButtonType.CANCEL);

      GridPane grid = new GridPane();
      int row = 0;

      ArrayList<TextField> matrixFields = new ArrayList<>();
      ArrayList<TextField> vectorFields = new ArrayList<>();
      ArrayList<TextField> coordsFields = new ArrayList<>();

      for (int i = 0; i < numTransformations; i++) {
        grid.add(new javafx.scene.control.Label("Matrix A" + (i + 1) + ": "), 0, row);
        TextField matrixField = new TextField();
        matrixField.setTextFormatter(new TextFormatter<>(InputFormatter.multipleInputsFilter()));
        matrixField.setPromptText("a00, a01, a10, a11");
        grid.add(matrixField, 1, row++);
        matrixFields.add(matrixField);

        grid.add(new javafx.scene.control.Label("Vector b" + (i + 1) + ": "), 0, row);
        TextField vectorField = new TextField();
        vectorField.setTextFormatter(new TextFormatter<>(InputFormatter.multipleInputsFilter()));
        vectorField.setPromptText("b0, b1");
        grid.add(vectorField, 1, row++);
        vectorFields.add(vectorField);

      }

      grid.add(new Label("Minimum coordinates 1: "), 0, row);
      TextField minCoords1 = new TextField();
      minCoords1.setTextFormatter(new TextFormatter<>(InputFormatter.numericalFilter()));
      grid.add(minCoords1, 1, row++);
      coordsFields.add(minCoords1);
      grid.add(new Label("Minimum coordinates 2: "), 0, row);
      TextField minCoords2 = new TextField();
      minCoords2.setTextFormatter(new TextFormatter<>(InputFormatter.numericalFilter()));
      grid.add(minCoords2, 1,  row++);
      coordsFields.add(minCoords2);
      grid.add(new Label("Maximum coordinates 1: "), 0, row);
      TextField maxCoords1 = new TextField();
      maxCoords1.setTextFormatter(new TextFormatter<>(InputFormatter.numericalFilter()));
      grid.add(maxCoords1, 1, row++);
      coordsFields.add(maxCoords1);
      grid.add(new Label("Maximum coordinates 2: "), 0, row);
      TextField maxCoords2 = new TextField();
      maxCoords2.setTextFormatter(new TextFormatter<>(InputFormatter.numericalFilter()));
      grid.add(maxCoords2, 1, row);
      coordsFields.add(maxCoords2);

      ArrayList<Transform2D> listOfTransforms = new ArrayList<>();

      dialog.getDialogPane().setContent(grid);

      // Declare boolean variables outside of listeners
      boolean[] coordsFilled = {false};
      boolean[] matricesFilled = {false};
      boolean[] vectorsFilled = {false};
      // Define a method to update the OK button state
      Runnable updateOkButtonState = () -> {
        boolean allFilled = coordsFilled[0] && matricesFilled[0] && vectorsFilled[0];
        dialog.getDialogPane().lookupButton(okButtonType).setDisable(!allFilled);
      };

      // Add a listener to each text field to enable/disable the OK button
      coordsFields.forEach(field -> field.textProperty().addListener(
          (observable, oldValue, newValue) -> {
            coordsFilled[0] = coordsFields.stream().noneMatch(
                f -> f.getText().trim().isEmpty());
            updateOkButtonState.run();
          }));
      matrixFields.forEach(field -> field.textProperty().addListener(
          (observable, oldValue, newValue) -> {
            matricesFilled[0] = matrixFields.stream().allMatch(
                f -> f.getText().split(",").length == 4);
            updateOkButtonState.run();
          }));
      vectorFields.forEach(field -> field.textProperty().addListener(
          (observable, oldValue, newValue) -> {
            vectorsFilled[0] = vectorFields.stream().allMatch(
                f -> f.getText().split(",").length == 2);
            updateOkButtonState.run();
          }));

      // Initially disables the button
      updateOkButtonState.run();

      dialog.setResultConverter(dialogButton -> {
        if (dialogButton == okButtonType) {

          for (int i = 0; i < numTransformations; i++) {
            String[] matrixElements = matrixFields.get(i).getText().split(",");
            double a00 = Double.parseDouble(matrixElements[0].trim());
            double a01 = Double.parseDouble(matrixElements[1].trim());
            double a10 = Double.parseDouble(matrixElements[2].trim());
            double a11 = Double.parseDouble(matrixElements[3].trim());
            Matrix2x2 matrixA = new Matrix2x2(a00, a01, a10, a11);

            String[] vectorElements = vectorFields.get(i).getText().split(",");
            double b0 = Double.parseDouble(vectorElements[0].trim());
            double b1 = Double.parseDouble(vectorElements[1].trim());
            Vector2D vectorb = new Vector2D(b0, b1);

            listOfTransforms.add(new AffineTransform2D(matrixA, vectorb));
          }
          double min1 = Double.parseDouble(minCoords1.getText());
          double min2 = Double.parseDouble(minCoords2.getText());
          double max1 = Double.parseDouble(maxCoords1.getText());
          double max2 = Double.parseDouble(maxCoords2.getText());
          ChaosGameDescription description = new ChaosGameDescription(
              listOfTransforms, new Vector2D(min1, min2), new Vector2D(max1, max2));
          chaosGame.updateChaosGame(description);
          ChaosGameFileHandler.writeToFile(description, "src/main/resources");
        }
        return null;
      });

      dialog.showAndWait();
    });
  }
}
