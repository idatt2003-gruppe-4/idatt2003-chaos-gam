package edu.ntnu.stud.view;

import edu.ntnu.stud.model.game.ChaosGame;
import edu.ntnu.stud.model.game.ChaosGameDescription;
import edu.ntnu.stud.model.game.ChaosGameFileHandler;
import java.io.FileNotFoundException;
import javafx.application.Application;
import javafx.geometry.Rectangle2D;
import javafx.stage.Screen;
import javafx.stage.Stage;

/**
 * The {@code ChaosGameMain} class serves as the entry point for the Chaos Game application.
 * It initializes the primary stage, reads the game description from a file,
 * creates a ChaosGame instance, and initializes the application window.
 */
public class ChaosGameMain extends Application {

  /**
   * Starts the Chaos Game application.
   *
   * @param primaryStage the primary stage of the application.
   * @throws FileNotFoundException if the file containing the game description is not found.
   */
  @Override
  public void start(Stage primaryStage) throws FileNotFoundException {
    Screen screen = Screen.getPrimary();
    Rectangle2D bounds = screen.getBounds();
    double screenHeight = bounds.getHeight();
    primaryStage.setMinHeight(614);
    primaryStage.setMinWidth(600);

    ChaosGameDescription description = ChaosGameFileHandler.readFromFile(
        "src/main/resources/Triangle.txt");
    ChaosGame chaosGame = new ChaosGame(description, 900, (int) screenHeight - 220);
    View window = new View(primaryStage, chaosGame);
    window.init();

    chaosGame.attach(window);
    primaryStage.show();

  }

  /**
   * Launches the application.
   *
   * @param args the command-line arguments.
   */
  public static void main(String[] args) {
    launch(args);
  }
}
