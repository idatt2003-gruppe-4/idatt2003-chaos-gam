package edu.ntnu.stud.view.components;

import edu.ntnu.stud.controller.InputFormatter;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.control.TextFormatter;
import javafx.scene.layout.GridPane;

/**
 * The {@code JuliaParameterFields} class provides a method
 * to create a {@link GridPane} containing text fields for
 * inputting the parameters required for a Julia transformation.
 */
public class JuliaParameterFields {

  /**
   * Creates and returns a {@link GridPane} with text fields for inputting the parameters
   * for a Julia transformation.
   *
   * @return a {@link GridPane} containing labeled text fields for real and imaginary numbers,
   *         and minimum and maximum coordinates.
   */
  public static GridPane juliaParameters() {

    TextField realNumber = new TextField();
    realNumber.setPromptText("Real number");
    realNumber.setTextFormatter(new TextFormatter<>(InputFormatter.numericalFilter()));
    TextField imaginaryNumber = new TextField();
    imaginaryNumber.setTextFormatter(new TextFormatter<>(InputFormatter.numericalFilter()));
    imaginaryNumber.setPromptText("Imaginary number");
    TextField minCoords1 = new TextField();
    minCoords1.setTextFormatter(new TextFormatter<>(InputFormatter.numericalFilter()));
    minCoords1.setPromptText("Min coordinates 1");
    TextField minCoords2 = new TextField();
    minCoords2.setPromptText("Min coordinates 2");
    minCoords2.setTextFormatter(new TextFormatter<>(InputFormatter.numericalFilter()));
    TextField maxCoords1 = new TextField();
    maxCoords1.setPromptText("Max coordinates 1");
    maxCoords1.setTextFormatter(new TextFormatter<>(InputFormatter.numericalFilter()));
    TextField maxCoords2 = new TextField();
    maxCoords2.setPromptText("Max coordinates 2");
    maxCoords2.setTextFormatter(new TextFormatter<>(InputFormatter.numericalFilter()));

    GridPane grid = new GridPane();

    grid.add(new Label("Real number:"), 0, 0);
    grid.add(realNumber, 1, 0);
    grid.add(new Label("Imaginary number:"), 0, 1);
    grid.add(imaginaryNumber, 1, 1);
    grid.add(new Label("Min coordinates 1: "), 0, 2);
    grid.add(minCoords1, 1, 2);
    grid.add(new Label("Min coordinates 2: "), 0, 3);
    grid.add(minCoords2, 1, 3);
    grid.add(new Label("Max coordinates 1: "), 0, 4);
    grid.add(maxCoords1, 1, 4);
    grid.add(new Label("Max coordinates 2: "), 0, 5);
    grid.add(maxCoords2, 1, 5);

    return grid;
  }
}