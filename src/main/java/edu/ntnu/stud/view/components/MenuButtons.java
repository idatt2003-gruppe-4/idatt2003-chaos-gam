package edu.ntnu.stud.view.components;

import edu.ntnu.stud.controller.AffineTransformDialog;
import edu.ntnu.stud.controller.ChaosGameDescriptionFactory;
import edu.ntnu.stud.controller.JuliaTransformDialog;
import edu.ntnu.stud.model.game.ChaosGame;
import javafx.scene.control.MenuButton;
import javafx.scene.control.MenuItem;
import javafx.scene.control.Tooltip;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.util.Duration;

/**
 * The {@code MenuButtons} class creates menu buttons for selecting predefined fractals
 * and for creating custom fractals using transformations.
 */
public class MenuButtons {
  private final MenuButton fractalOptionsButton;
  private final MenuButton createFractalButton;

  /**
   * Constructs a {@code MenuButtons} instance and initializes
   * the menu buttons with their respective functionalities and tooltips.
   *
   * @param chaosGame the {@link ChaosGame} instance used for creating and modifying fractals.
   */
  public MenuButtons(ChaosGame chaosGame) {

    fractalOptionsButton = new MenuButton();
    Image image1 = new Image(getImagePath("/images/MenuButtons.png"));
    setupImageView(image1, "menu-button-gear", fractalOptionsButton);

    MenuItem item1 = new MenuItem("Julia");
    item1.setOnAction(ChaosGameDescriptionFactory.juliaListener);
    MenuItem item2 = new MenuItem("Barnsley");
    item2.setOnAction(ChaosGameDescriptionFactory.barnsleyListener);
    MenuItem item3 = new MenuItem("Sierpinski");
    item3.setOnAction(ChaosGameDescriptionFactory.triangleListener);
    fractalOptionsButton.getItems().addAll(item1, item2, item3);
    fractalOptionsButton.setText("Fractal Options");

    Tooltip fractalOptionsTooltip = new Tooltip("Choose from a selection of predefined fractals"
         + " (Julia, Barnsley, Sierpinski)");
    fractalOptionsTooltip.setShowDelay(Duration.ZERO);
    Tooltip.install(fractalOptionsButton, fractalOptionsTooltip);


    createFractalButton = new MenuButton();
    Image image2 = new Image(getImagePath("/images/Hammer.png"));
    setupImageView(image2, "menu-button-gear", createFractalButton);

    MenuItem item4 = new MenuItem("Affine Transformations");
    item4.setOnAction(event -> new AffineTransformDialog(chaosGame));
    MenuItem item5 = new MenuItem("Julia Transformations");
    item5.setOnAction(event -> new JuliaTransformDialog(chaosGame));

    createFractalButton.getItems().addAll(item4, item5);
    createFractalButton.setText("Create Fractal");

    Tooltip createFractalTooltip = new Tooltip("Choose from a selection of transformations "
        + "to create  your own  fractal");
    createFractalTooltip.setShowDelay(Duration.ZERO);
    Tooltip.install(createFractalButton, createFractalTooltip);
  }

  /**
   * Retrieves the path to the specified image resource.
   *
   * @param path the relative path to the image resource.
   * @return the absolute path to the image resource.
   */
  private String getImagePath(String path) {
    return getClass().getResource(path).toExternalForm();
  }

  /**
   * Sets up the image view for a menu button.
   *
   * @param image the image to be displayed on the button.
   * @param styleClass the style class to be applied to the button.
   * @param button the menu button to which the image view is added.
   */
  private void setupImageView(Image image, String styleClass, MenuButton button) {
    ImageView imageView = new ImageView(image);
    imageView.setFitHeight(45);
    imageView.setFitWidth(45);
    button.setGraphic(imageView);
    button.getStyleClass().add(styleClass);
  }

  /**
   * Retrieves the menu button for fractal options.
   *
   * @return the menu button for fractal options.
   */
  public MenuButton getFractalOptionsButton() {
    return fractalOptionsButton;
  }

  /**
   * Retrieves the menu button for creating fractals.
   *
   * @return the menu button for creating fractals.
   */
  public MenuButton getCreateFractalButton() {
    return createFractalButton;
  }
}