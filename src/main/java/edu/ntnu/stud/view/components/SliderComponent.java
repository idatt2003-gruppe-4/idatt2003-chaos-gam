package edu.ntnu.stud.view.components;

import edu.ntnu.stud.model.game.ChaosGame;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Label;
import javafx.scene.control.Slider;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;

/**
 * The {@code SliderComponent} class creates a slider
 * component for adjusting the number of steps in the chaos game.
 */
public class SliderComponent {
  private final Slider slider;
  private final Label sliderValue;
  private static HBox sliderContainer;

  /**
   * Constructs a {@code SliderComponent} instance and initializes
   * the slider component with its default values and behavior.
   *
   * @param chaosGame the {@link ChaosGame} instance used for updating the number of steps.
   */
  public SliderComponent(ChaosGame chaosGame) {

    slider = new Slider();
    slider.setMin(100);
    slider.setMax(200000);
    slider.setValue(1500);
    slider.setMajorTickUnit(10);
    slider.setSnapToTicks(true);
    slider.setPrefWidth(500);
    slider.setPadding(new Insets(30, 100, 30, 0));

    sliderValue = new Label("Steps: " + (int) slider.getValue());
    sliderValue.setPadding(new Insets(30, 50, 30, 0));
    slider.valueProperty().addListener(new ChangeListener<Number>() {
      @Override
      public void changed(ObservableValue<? extends Number>
                              observableValue, Number number,  Number t1) {
        sliderValue.setText("Steps: " + (int) slider.getValue());
        chaosGame.updateSteps((int) slider.getValue());
      }
    });

    sliderContainer = new HBox(sliderValue, slider);
    sliderContainer.setAlignment(Pos.CENTER_RIGHT);
    sliderContainer.setStyle("-fx-background-color: #ffffff");
    HBox.setHgrow(sliderContainer, Priority.ALWAYS);
  }

  /**
   * Returns the HBox containing the slider component.
   *
   * @return the HBox containing the slider component.
   */
  public HBox getSliderContainer() {
    return sliderContainer;
  }
}
