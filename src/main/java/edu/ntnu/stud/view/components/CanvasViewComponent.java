package edu.ntnu.stud.view.components;

import edu.ntnu.stud.model.game.ChaosGame;
import edu.ntnu.stud.model.game.ChaosGameDescription;
import edu.ntnu.stud.model.game.ChaosGameFileHandler;
import java.io.FileNotFoundException;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.geometry.Rectangle2D;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.layout.HBox;
import javafx.scene.paint.Color;
import javafx.stage.Screen;
import javafx.stage.Stage;

/**
 * The {@code CanvasViewComponent} class is responsible for creating and managing a canvas
 * where the fractal is visualized. It handles dynamic resizing of the canvas based on the
 * stage's dimensions and updates the canvas content accordingly.
 */
public class CanvasViewComponent {
  private final Canvas canvas;
  private final GraphicsContext gc;
  private final HBox hbox = new HBox();
  private static ChaosGame chaosGame;
  private static ChaosGameDescription description;

  /**
   * Constructs a {@code CanvasViewComponent} with a specified {@link Stage} and {@link ChaosGame}.
   * Initializes the canvas to fit the screen dimensions and sets up listeners for dynamic resizing.
   *
   * @param stage the {@link Stage} on which the canvas is displayed.
   * @param chaosGame the {@link ChaosGame} instance used for generating the game content.
   * @throws FileNotFoundException if the initial description file is not found.
   */
  public CanvasViewComponent(Stage stage, ChaosGame chaosGame) throws FileNotFoundException {

    // Get the primary screen
    Screen screen = Screen.getPrimary();

    // Get the bounds of the primary screen
    Rectangle2D bounds = screen.getBounds();

    // Get the width of the primary screen
    double screenWidth = bounds.getWidth();
    double screenHeight = bounds.getHeight();
    canvas = new Canvas(screenWidth, screenHeight - 250);
    gc = canvas.getGraphicsContext2D();
    String filePath = "src/main/resources/Triangle.txt";
    description = ChaosGameFileHandler.readFromFile(filePath);
    this.chaosGame = chaosGame;
    // Add listeners to the stage's width and height properties for dynamic canvas resizing
    stage.widthProperty().addListener(new ChangeListener<Number>() {
      @Override
      public void changed(ObservableValue<? extends Number>
                              observableValue, Number oldWidth, Number newWidth) {
        canvas.setWidth(newWidth.doubleValue());
        try {
          drawCanvas();
        } catch (FileNotFoundException e) {
          throw new RuntimeException(e);
        }
      }
    });

    stage.heightProperty().addListener(new ChangeListener<Number>() {
      @Override
      public void changed(ObservableValue<? extends Number>
                              observableValue, Number oldHeight, Number newHeight) {
        canvas.setHeight(newHeight.doubleValue() - 200);
        try {
          drawCanvas();
        } catch (FileNotFoundException e) {
          throw new RuntimeException(e);
        }
      }
    });
  }

  /**
   * Draws the canvas by clearing it first and then filling it with
   * the Chaos Game's current state.
   * The Chaos Game's canvas array is used to determine the pixels to be filled.
   *
   * @throws FileNotFoundException if the description file for the Chaos Game is not found.
   */
  public void drawCanvas() throws FileNotFoundException {
    gc.clearRect(0, 0, canvas.getWidth(), canvas.getHeight());
    chaosGame.getCanvas().clear();
    chaosGame.runSteps();
    int[][] matrix = chaosGame.getCanvas().getCanvasArray();
    int numRows = matrix.length;
    int numCols = matrix[0].length;

    // Iterate through the matrix and fill pixels based on the values
    for (int i = 0; i < numRows; i++) {
      for (int j = 0; j < numCols; j++) {
        if (matrix[i][j] == 1) {
          gc.setFill(Color.WHITE);
          gc.fillRect(j + canvas.getWidth() / 2 - 450, i, 1, 1);
        }
      }
    }
  }

  /**
   * Updates the Chaos Game description file and the Chaos Game instance with new content.
   *
   * @param filePath the path to the new description file.
   * @throws FileNotFoundException if the specified file is not found.
   */
  public static void updateFile(String filePath) throws FileNotFoundException {
    description = ChaosGameFileHandler.readFromFile(filePath);
    chaosGame.updateChaosGame(description);
  }

  /**
   * Retrieves the HBox containing the canvas.
   * If the canvas is not already added to the HBox, it is added.
   *
   * @return the HBox containing the canvas.
   */
  public HBox getCanvasHBox() {
    if (!hbox.getChildren().contains(canvas)) {
      hbox.getChildren().add(canvas);
    }
    return hbox;
  }
}
