package edu.ntnu.stud.view.components;

import edu.ntnu.stud.controller.ParameterDialog;
import edu.ntnu.stud.model.game.ChaosGame;
import edu.ntnu.stud.model.transformation.Transform2D;
import javafx.scene.control.Button;
import javafx.scene.control.Tooltip;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.util.Duration;

/**
 * The {@code GearButton} class represents a button that, when clicked,
 * opens a parameter dialog allowing the user to change
 * various parameters of the {@link ChaosGame} visualization.
 */
public class GearButton extends Button {

  /**
   * Constructs a {@code GearButton} with the specified {@link ChaosGame}.
   * Sets up the button's image, tooltip, and action handler.
   *
   * @param chaosGame the {@link ChaosGame} instance used for the parameter dialog.
   */
  public GearButton(ChaosGame chaosGame) {
    Image image = new Image(getImagePath());
    setupImageView(image, "button-gear");

    Tooltip tooltip = new Tooltip("Change various parameters to see how the visualization "
        + "of the fractal changes");
    tooltip.setShowDelay(Duration.ZERO);
    Tooltip.install(this, tooltip);

    this.setOnAction(event -> {
      ParameterDialog parameterDialog = new ParameterDialog(chaosGame);
      Transform2D transform = chaosGame.getTransform(); // get or create the Transform2D object
      parameterDialog.handleTransform(transform);
    });
  }

  /**
   * Gets the image path for the button's icon.
   *
   * @return the absolute path to the image resource.
   */
  private String getImagePath() {
    return getClass().getResource("/images/GearButton.png").toExternalForm();
  }

  /**
   * Sets up the {@link ImageView} for the button with the specified image and style class.
   *
   * @param image the {@link Image} to be displayed on the button.
   * @param styleClass the CSS style class to be applied to the button.
   */
  private void setupImageView(Image image, String styleClass) {
    ImageView imageView = new ImageView(image);
    imageView.setFitHeight(50);
    imageView.setFitWidth(50);
    this.setGraphic(imageView);
    this.getStyleClass().add(styleClass);
  }
}