package edu.ntnu.stud.view.components;

import edu.ntnu.stud.model.game.ChaosGame;
import javafx.geometry.Pos;
import javafx.scene.control.Separator;
import javafx.scene.control.ToolBar;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.Region;
import javafx.scene.layout.VBox;

/**
 * The {@code TopBar} class represents the top bar of the application's user interface.
 * It contains menu buttons for selecting fractal options and creating fractals,
 * as well as a gear button for accessing additional settings.
 */
public class TopBar extends VBox {

  /**
   * Constructs a {@code TopBar} instance with the specified {@link ChaosGame}.
   *
   * @param chaosGame the {@link ChaosGame} instance used in the application.
   */
  public TopBar(ChaosGame chaosGame) {

    MenuButtons menuButtons = new MenuButtons(chaosGame);

    HBox menuButtonsBox = new HBox();
    menuButtonsBox.getChildren().addAll(
        menuButtons.getFractalOptionsButton(), menuButtons
            .getCreateFractalButton());
    menuButtonsBox.setSpacing(50);
    menuButtonsBox.getStyleClass().add("vbox-style");


    Region spacer = new Region();
    HBox.setHgrow(spacer, Priority.ALWAYS);
    spacer.getStyleClass().add("vbox-style");

    GearButton gearButton = new GearButton(chaosGame);

    HBox gearButtonBox = new HBox();
    gearButtonBox.getChildren().add(gearButton);
    gearButtonBox.setAlignment(Pos.TOP_RIGHT);
    gearButtonBox.getStyleClass().add("vbox-style");


    ToolBar toolBar = new ToolBar();
    toolBar.getItems().addAll(menuButtonsBox, spacer, gearButtonBox);
    toolBar.getStyleClass().add("vbox-style");


    Separator separator = new Separator();


    this.getChildren().addAll(toolBar, separator);


    menuButtons.getFractalOptionsButton().getStyleClass().add("button-style");
    menuButtons.getCreateFractalButton().getStyleClass().add("button-style");
    gearButton.getStyleClass().add("button-style");
    this.getStyleClass().add("vbox-style");
  }


  /**
   * Retrieves the {@code TopBar} instance.
   *
   * @return the {@code TopBar} instance.
   */
  public TopBar getTopBar() {
    return this;
  }
}