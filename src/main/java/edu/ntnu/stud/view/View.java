package edu.ntnu.stud.view;

import edu.ntnu.stud.model.ChaosGameObserver;
import edu.ntnu.stud.model.game.ChaosGame;
import edu.ntnu.stud.view.components.CanvasViewComponent;
import edu.ntnu.stud.view.components.SliderComponent;
import edu.ntnu.stud.view.components.TopBar;
import java.io.FileNotFoundException;
import javafx.scene.Scene;
import javafx.scene.layout.*;
import javafx.stage.Stage;

/**
 * The {@code View} class represents the graphical user interface of the Chaos Game application.
 * It contains components such as the canvas view, top bar, and slider component.
 * Implements {@link ChaosGameObserver} to observe changes in the Chaos Game state.
 */
public class View implements ChaosGameObserver {
  private static BorderPane root = new BorderPane();
  private static CanvasViewComponent canvasViewComponent;
  private TopBar topBar;
  private SliderComponent sliderComponent;

  /**
   * Constructs a new View object.
   *
   * @param primaryStage the primary stage of the application.
   * @param chaosGame    the Chaos Game instance.
   * @throws FileNotFoundException if the file for initializing components is not found.
   */
  public View(Stage primaryStage, ChaosGame chaosGame) throws FileNotFoundException {
    Scene scene = new Scene(root);
    primaryStage.setScene(scene);
    primaryStage.setMaximized(true);
    primaryStage.setTitle("Chaos Game");
    root.setStyle("-fx-background-color: BLACK");
    scene.getStylesheets().add(getClass().getResource("/styles/topbar.css").toExternalForm());
    canvasViewComponent = new CanvasViewComponent(primaryStage, chaosGame);
    topBar = new TopBar(chaosGame);
    sliderComponent = new SliderComponent(chaosGame);
  }

  /**
   * Initializes the view components and displays them on the stage.
   *
   * @throws FileNotFoundException if the file for initializing components is not found.
   */
  public void init() throws FileNotFoundException {
    canvasViewComponent.drawCanvas();
    root.setTop(topBar.getTopBar());
    root.setCenter(canvasViewComponent.getCanvasHBox());
    root.setBottom(sliderComponent.getSliderContainer());
  }

  /**
   * Updates the view in response to changes in the Chaos Game state.
   */
  @Override
  public void update() {
    try {
      canvasViewComponent.drawCanvas();
    }  catch (FileNotFoundException e) {
      System.out.println("Cannot find the file!");
    }
    root.getChildren().set(1, canvasViewComponent.getCanvasHBox());
  }
}
